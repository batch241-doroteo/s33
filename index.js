
fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then((json) => console.log(json.map(function (obj){
    let todoObject = {
        "title": obj.title
    }
    return Object.values(todoObject).toString() })))
/////////////////////////////////////

fetch('https://jsonplaceholder.typicode.com/todos', {

	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},

	body: JSON.stringify({
        "userId": 1,
        "id": 1,
        "title": "delectus aut autem",
        "completed": false
	})
})
.then(response => response.json())
.then((json) => console.log(json))

//////////////////////////////////////

fetch('https://jsonplaceholder.typicode.com/todos/1', {

	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},

	body: JSON.stringify({
		"completed": false,
        "title": "Created To Do List Item",
        "userId": 1,
	})
})
.then(response => response.json())
.then((json) => console.log(json))

///////////////////////////////

fetch('https://jsonplaceholder.typicode.com/todos/1', {

	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},

	body: JSON.stringify({
        "userId": 1,
        "id": 1,
        "title": "To update the my to do list with a different data structure",
        "dateCompleted": "Pending"
	})
})
.then(response => response.json())
.then((json) => console.log(json))

///////////////////////////////

fetch('https://jsonplaceholder.typicode.com/todos/1', {

	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},

	body: JSON.stringify({
		"completed": false,
		"dateCompleted": "07/09/21",
        "id": 1,
        "status": "Complete",
        "title": "delectus aut autem",
        "userId": 1
	})
})
.then(response => response.json())
.then((json) => console.log(json))







